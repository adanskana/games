;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages libpng)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix utils)
  #:use-module (gnu packages image))

(define-public libpng-1.2                                                       
  (package                                                                      
    (inherit libpng)                                                            
    (version "1.2.59")                                                          
    (source                                                                     
     (origin                                                                    
       (method url-fetch)                                                       
       (uri (list (string-append "mirror://sourceforge/libpng/libpng12/"        
                                 version "/libpng-" version ".tar.xz")          
                  (string-append                                                
                   "ftp://ftp.simplesystems.org/pub/libpng/png/src"             
                   "/libpng12/libpng-" version ".tar.xz")                       
                  (string-append                                                
                   "ftp://ftp.simplesystems.org/pub/libpng/png/src/history"     
                   "/libpng12/libpng-" version ".tar.xz")))                     
       (sha256                                                                  
        (base32                                                                 
         "1izw9ybm27llk8531w6h4jp4rk2rxy2s9vil16nwik5dp0amyqxl"))))))
