;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Sughosha <sughosha@proton.me>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages iratus)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (games build-system mojo)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xorg)
  #:use-module (nonguix build utils)
  #:use-module (games gog-download))

(define-public gog-iratus-lord-of-the-dead
  (let ((buildno "48040"))
    (package
      (name "gog-iratus-lord-of-the-dead")
      (version "181.13.00")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://iratus_lord_of_the_dead/en3installer0")
         (file-name (string-append "iratus_lord_of_the_dead_"
                                  (string-replace-substring version "." "_")
                                  "_" buildno ".sh"))
         (sha256
          (base32
           "0dyn2c5mzvji6bymzy1pp6kq1zbq3d78hb6px73jwphjbl62hsvi"))))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         `(("Iratus"
            ("libc" "gcc:lib" "alsa-lib" "eudev" "libx11" "libxcursor"
             "libxext" "libxi" "libxinerama" "libxrandr" "libxscrnsaver"
             "libxxf86vm" "mesa" "pulseaudio" "vulkan-loader"))
           ("Iratus_Data/Mono/x86_64/libmono.so"
             ("libc"))
           ("Iratus_Data/Mono/x86_64/libMonoPosixHelper.so"
             ("libc" "zlib"))
           ("Iratus_Data/Plugins/x86_64/libsteam_api.so"
             ("libc"))
           ("Iratus_Data/Plugins/x86_64/ScreenSelector.so"
             ("libc" "gcc:lib" "glib" "gdk-pixbuf" "gtk+")))))
      (inputs `(("alsa-lib" ,alsa-lib)
                ("eudev" ,eudev)
                ("gcc:lib" ,gcc "lib")
                ("gdk-pixbuf" ,gdk-pixbuf)
                ("gtk+" ,gtk+-2)
                ("libx11" ,libx11)
                ("libxcursor" ,libxcursor)
                ("libxext" ,libxext)
                ("libxi" ,libxi)
                ("libxinerama" ,libxinerama)
                ("libxrandr" ,libxrandr)
                ("libxscrnsaver" ,libxscrnsaver)
                ("libxxf86vm" ,libxxf86vm)
                ("mesa" ,mesa)
                ("pulseaudio" ,pulseaudio)
                ("vulkan-loader" ,vulkan-loader)
                ("zlib" ,zlib)))
      (supported-systems '("x86_64-linux"))
      (home-page "https://iratus.org/")
      (synopsis "Turn-based role-playing game")
      (description "In Iratus you find yourself fighting for the forces of
darkness in the role of the titular necromancer—Iratus, recently freed from his
millennia-long imprisonment.  You control an obedient army of the living dead,
with skeletons, zombies, banshees and many other unliving warriors.")
      (license (undistributable
                (string-append "file:///data/noarch/docs/"
                               "End User License Agreement.txt"))))))
