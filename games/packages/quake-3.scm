;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages quake-3)
  #:use-module (ice-9 match)
  #:use-module (games utils)
  #:use-module (nonguix licenses)
  #:use-module (guix packages)
  #:use-module (guix build-system trivial)
  #:use-module (guix build utils)
  #:use-module (games game-local-fetch)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages game-development))

(define quake-3-patch-data ; TODO: Use URL upstream, but need to support --referer first.
  (origin
    (method game-local-fetch)
    (uri "quake-3-patch-data")
    ;; (uri "https://www.ioquake3.org/data/quake3-latest-pk3s.zip")
    (sha256 #f)))

(define-public quake-3-arena
  (package
    (name "quake-3-arena")
    (version (package-version ioquake3))
    (source
     (origin
       (method game-local-fetch)
       (uri name)
       (sha256 #f)))
    (build-system trivial-build-system)
    (native-inputs
     `(("ioquake3-source" ,(package-source ioquake3))
       ("patch-data" ,quake-3-patch-data)
       ("p7zip" ,p7zip)))
    (propagated-inputs
     `(("ioquake3" ,ioquake3)))
    (arguments
     `(#:modules ((guix build utils)
                  (nonguix build utils)
                  (srfi srfi-1)
                  (srfi srfi-26))
       #:builder
       (begin
         (use-modules (guix build utils)
                      (nonguix build utils)
                      (srfi srfi-1)
                      (srfi srfi-26))
         (let* ((out (assoc-ref %outputs "out"))
                (bin (string-append out "/bin/baseq3")))
           (setenv "PATH" (string-append
                           (assoc-ref %build-inputs "p7zip") "/bin"))
           (mkdir-p bin)
           (chdir bin)
           (invoke "7z" "e" (assoc-ref %build-inputs "source") "Quake3/baseq3/pak0.pk3")
           ;; Install patch.
           (apply invoke "7z" "e" (assoc-ref %build-inputs "patch-data")
                  (map (cut format "quake3-latest-pk3s/baseq3/pak~a.pk3" <>)
                       (iota 8 1)))
           ;; Install desktop files.
           (make-desktop-entry-file (string-append out "/share/applications/quake3.desktop")
                                    #:name "Quake III Arena"
                                    #:exec (string-append out "/bin/ioquake3.x86_64")
                                    #:icon (string-append out "/share/pixmaps/quake3.png")
                                    #:categories '("Application" "Game"))
           (install-file
            (string-append (assoc-ref %build-inputs "ioquake3-source") "/misc/quake3.png")
            (string-append out "/share/pixmaps/"))))))
    (home-page "https://ioquake3.org/") ; Non-official, but most relevant?
    (synopsis "Fast-paced, multiplayer-focused first-person shooter")
    (description "Multiplayer-focused first-person shooter video game developed
by id Software.  Quake III Arena is the third game in the Quake series and
differs from previous games by excluding a traditional single-player element,
instead focusing on multiplayer action.  The single-player mode is played
against computer-controlled bots.

Notable features of Quake III Arena include the minimalist design, lacking
rarely used items and features, the extensive customizability of player settings
such as field of view, texture detail and enemy model, and advanced movement
features such as strafe-jumping and rocket-jumping.  It features music composed
by Sonic Mayhem and Front Line Assembly founder, Bill Leeb.

The game progress is saved in ~/.q3a/baseq3/q3config.cfg and mods can be
installed locally by copying the .pk3 files to ~/.q3a/baseq3/.")
    (supported-systems '("i686-linux" "x86_64-linux"))
    (license (undistributable "No URL"))))

(define-public quake-3-team-arena
  (package
    (inherit quake-3-arena)
    (name "quake-3-team-arena")
    (source
     (origin
       (method game-local-fetch)
       (uri name)
       (sha256 #f)))
    (native-inputs
     `(("p7zip" ,p7zip)
       ("patch-data" ,quake-3-patch-data)))
    (propagated-inputs
     `(("quake-3-arena" ,quake-3-arena)))
    (arguments
     `(#:modules ((guix build utils)
                  (srfi srfi-1)
                  (srfi srfi-26))
       #:builder
       (begin
         (use-modules (guix build utils)
                      (srfi srfi-1)
                      (srfi srfi-26))
         (let* ((out (assoc-ref %outputs "out"))
                (bin (string-append out "/bin/missionpack")))
           (setenv "PATH" (string-append
                           (assoc-ref %build-inputs "p7zip") "/bin"))
           (mkdir-p bin)
           (chdir bin)
           (invoke "7z" "e" (assoc-ref %build-inputs "source")
                   "Setup/missionpack/PAK0.PK3")
           ;; Install patch.
           (apply invoke "7z" "e" (assoc-ref %build-inputs "patch-data")
                  (map (cut format "quake3-latest-pk3s/missionpack/pak~a.pk3" <>)
                       (iota 3 1)))))))
    (synopsis "Extension to Quake 3 Arena")
    (description "Also known as \"Mission Pack\", this expansion is focused on
team-based gameplay through new game modes, as well as the addition of three new
weapons (the Chaingun, Nailgun, and Prox Launcher), and new items and player
models. ")))
