;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages factorio)
  #:use-module (games utils)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix download)
  #:use-module (nonguix licenses)
  #:use-module (nonguix multiarch-container)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg))

;; This file is heavily inspired by the corresponding package in Nixpkgs.

(define factorio-help
  (string-append
    "FETCH FAILED
============

Please ensure you have set the username and token in `"
    (guix-gaming-channel-games-config)
    "`.
Your token can be seen at https://factorio.com/profile (after logging in).  It is
not as sensitive as your password, but should still be safeguarded.  There is a
link on that page to revoke/invalidate the token, if you believe it has been
leaked or wish to take precautions.

Example content for the file:

```scheme
((game1 ...)
 ...
 (factorio .
   ((username . \"johnsmith\")
    (token    . \"c40c9636e1ba61261a50e49a035fff1d\")))
 ...
 (gameN ...))
```"))

(define stable-version "1.1.109")
(define experimental-version "1.1.109")

(define* (factorio-url-fetch url hash-algo hash
                            #:optional name
                            #:key (system (%current-system))
                            (guile (default-guile)))
  "Return a fixed-output derivation that fetches URL (a string) which is
expected to have HASH of type HASH-ALGO (a symbol).  By default, the file name
is the base name of URL; optionally, NAME can specify a different file name.

This derivation will download URL and add the username and token as defined in
the configuration of the games channel."
  (define file-name (if name name (basename url)))
  (let* ((factorio-config (read-config factorio-help 'factorio))
         (username (assoc-ref factorio-config 'username))
         (token (assoc-ref factorio-config 'token)))
    (unredistributable-url-fetch
      (string-append url "?username=" username "&token=" token)
      hash-algo hash file-name #:system system #:guile guile)))

(define-public factorio-client
  (package
    (name "factorio-client")
    (version stable-version)
    (source (origin
             (method factorio-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 version "/alpha/linux64"))
             (file-name (string-append name "-" version ".tar.xz"))
             (sha256
              (base32
               "1fmgh5b4sq9lcbjz0asvq5zcwf25cqdn5jc2ickind2lnkhd557h"))))
    (build-system binary-build-system)
    (supported-systems '("x86_64-linux"))
    (arguments
     `(#:patchelf-plan
       '(("bin/x64/factorio" ("alsa-lib" "glibc" "libx11" "libxcursor"
                              "libxext" "libxinerama" "libxrandr" "mesa"
                              "pulseaudio")))
       #:install-plan '(("bin/x64/factorio" "libexec/factorio")
                        ("data" "share/factorio")
                        ("doc-html" "share/doc/factorio"))
       #:validate-runpath? #f
       #:strip-binaries? #f))
    (native-inputs
     `(("patchelf" ,patchelf)
       ("tar" ,tar)
       ("xz" ,xz)))
    (inputs
     `(("alsa-lib" ,alsa-lib)
       ("bash" ,bash)
       ("gcc" ,gcc "lib")
       ("glibc" ,glibc)
       ("libice" ,libice)
       ("libsm" ,libsm)
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("libxinerama" ,libxinerama)
       ("libxrandr" ,libxrandr)
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)))
    (home-page "https://factorio.com")
    (synopsis "Factory building game")
    (description "Factorio is a game in which you build and maintain factories.

You will be mining resources, researching technologies, building infrastructure,
automating production and fighting enemies.  Use your imagination to design your
factory, combine simple elements into ingenious structures, apply management
skills to keep it working, and protect it from the creatures who don't really
like you.")
    (license (undistributable "https://factorio.com/terms-of-service"))))

(define-public factorio-client-experimental
  (package
    (inherit factorio-client)
    (name "factorio-client-next")
    (version experimental-version)
    (source (origin
             (method factorio-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 version "/alpha/linux64"))
             (file-name (string-append "factorio-" version ".tar.xz"))
             (sha256
              (base32
               "1fmgh5b4sq9lcbjz0asvq5zcwf25cqdn5jc2ickind2lnkhd557h"))))))

(define-public factorio-client-headless
  (package
    (inherit factorio-client)
    (name "factorio-client-headless")
    (source (origin
             (method unredistributable-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 stable-version "/headless/linux64"))
             (file-name (string-append name "-" stable-version ".tar.xz"))
             (sha256
              (base32
               "0gxzfz074833fjm4s3528y05c5n1jf7zxfdj5xpfcvwi7i9khnhh"))))
    (arguments
     (substitute-keyword-arguments (package-arguments factorio-client)
       ((#:install-plan plan)
        `'(("bin/x64/factorio" "libexec/factorio")
           ("data" "share/factorio")))))))

(define-public factorio-client-headless-experimental
  (package
    (inherit factorio-client-headless)
    (name "factorio-client-headless-next")
    (version experimental-version)
    (source (origin
             (method unredistributable-url-fetch)
             (uri (string-append "https://factorio.com/get-download/"
                                 version "/headless/linux64"))
             (file-name (string-append "factorio-headless-" version ".tar.xz"))
             (sha256
              (base32
               "0gxzfz074833fjm4s3528y05c5n1jf7zxfdj5xpfcvwi7i9khnhh"))))))

(define factorio-container-libs
  '())
(define factorio-ld.so.conf
  (packages->ld.so.conf
   (list (fhs-union factorio-container-libs
                    #:name "fhs-union-64"))))
(define factorio-ld.so.cache
  (ld.so.conf->ld.so.cache factorio-ld.so.conf))

(define (factorio-container name package)
  (nonguix-container
    (name name)
    (wrap-package package)
    (run "/libexec/factorio")
    (exposed #~((#$(file-append package "/libexec") . "/usr/libexec")
                (#$(file-append package "/share/factorio") . "/usr/share/factorio")))
    (ld.so.conf factorio-ld.so.conf)
    (ld.so.cache factorio-ld.so.cache)
    (union64
     (fhs-union factorio-container-libs
                #:name "fhs-union-64"))
    (union32
     (fhs-union factorio-container-libs
                #:name "fhs-union-32"
                #:system "i686-linux"))
    (link-files '())
    (description (package-description factorio-client))))

(define-public factorio
  (nonguix-container->package
    (factorio-container "factorio" factorio-client)))
(define-public factorio-next
  (nonguix-container->package
    (factorio-container "factorio-next" factorio-client-experimental)))
(define-public factorio-headless
  (nonguix-container->package
    (factorio-container "factorio-headless" factorio-client-headless)))
(define-public factorio-headless-next
  (nonguix-container->package
    (factorio-container "factorio-headless-next" factorio-client-headless-experimental)))
