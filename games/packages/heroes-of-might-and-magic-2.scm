;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Eidvilas Markevičius <markeviciuseidvilas@gmail.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages heroes-of-might-and-magic-2)
  #:use-module (games gog-download)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages games)
  #:use-module (guix build-system copy)
  #:use-module (guix packages)
  #:use-module (nonguix licenses))

(define-public gog-heroes-of-might-and-magic-2
  (package
    (name "gog-heroes-of-might-and-magic-2")
    (version "2.0.0.7")
    (source
      (origin
        (method gog-fetch)
        (uri "gogdownloader://heroes_of_might_and_magic_2_gold_edition/2863")
        (file-name (string-append "gog-heroes-of-might-and-magic-2-" version ".exe"))
        (sha256 (base32 "1nav9zd33gsfyv5vlnbjnnxbcybyixqhbn10ra5y002x2pms0aqb"))))
    (native-inputs
      (list innoextract))
    (inputs
      (list coreutils fheroes2))
    (build-system copy-build-system)
    (arguments
      '(#:phases
        (modify-phases %standard-phases
          (replace 'unpack
            (lambda* (#:key source #:allow-other-keys)
              (invoke "innoextract"
                      "--lowercase"
                      "--include" "/app/heroes2/anim"
                      "--include" "/app/data"
                      "--include" "/app/maps"
                      "--include" "/app/music"
                      "--include" "/app/manual.pdf"
                      source)))
          (add-after 'install 'install-wrapper
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let ((coreutils (assoc-ref inputs "coreutils"))
                    (fheroes2 (assoc-ref inputs "fheroes2"))
                    (out (assoc-ref outputs "out")))
                (mkdir-p (string-append out "/bin"))
                (call-with-output-file (string-append out "/bin/homm2")
                  (lambda (port)
                    (display
                      (string-append
                        "#!/bin/sh\n\n"
                        "export XDG_DATA_HOME="
                          "\"${XDG_DATA_HOME:-$HOME/.local/share}/homm2\"\n"
                        "export XDG_CONFIG_HOME="
                          "\"${XDG_CONFIG_HOME:-$HOME/.config}/homm2\"\n"
                        "export PATH="
                          "\"" coreutils "/bin:" fheroes2 "/bin\"\n\n"
                        "mkdir -p -m=755 \"$XDG_DATA_HOME/fheroes2\"\n"
                        "ln -s -f "
                          "\"" out "/share/homm2\"/* "
                          "\"" "$XDG_DATA_HOME/fheroes2/\"\n"
                        "fheroes2\n")
                      port)))
                (chmod (string-append out "/bin/homm2") #o555))))
          (add-after 'install-wrapper 'install-icons
            (lambda* (#:key inputs outputs #:allow-other-keys)
              (let ((fheroes2 (assoc-ref inputs "fheroes2"))
                    (out (assoc-ref outputs "out")))
                (mkdir-p (string-append out "/share/icons/hicolor/128x128/apps"))
                (symlink (string-append fheroes2
                           "/share/icons/hicolor/128x128/apps/fheroes2.png")
                         (string-append out
                           "/share/icons/hicolor/128x128/apps/homm2.png")))))
          (add-after 'install-icons 'install-desktop-entry
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
                (make-desktop-entry-file
                  (string-append out "/share/applications/homm2.desktop")
                  #:name "Heroes of Might and Magic II"
                  #:comment "A turn-based strategy video game"
                  #:icon "homm2"
                  #:exec (string-append out "/bin/homm2")
                  #:categories '("Application" "Game"))))))
        #:install-plan
        '(("app/heroes2/anim" "share/homm2/")
          ("app/data" "share/homm2/")
          ("app/maps" "share/homm2/")
          ("app/music" "share/homm2/")
          ("app/manual.pdf" "share/doc/homm2/"))))
    (home-page "https://gog.com/game/heroes_of_might_and_magic_2_gold_edition")
    (synopsis "Heroes of Might and Magic II")
    (description "Heroes of Might and Magic II is a turn-based strategy video game
developed by Jon Van Caneghem through New World Computing and published in 1996 by the
3DO Company.  The game is the second instalment of the Heroes of Might and Magic series
and is typically credited as the breakout game for the series.  Heroes II was voted the
sixth-best PC game of all time by PC Gamer in May 1997.")
    (license (undistributable "No URL"))))
